// Uso de la sentencia switch
// Página 17 del libro Data Structures & Problem Solving Using Java
// etcheverrypablol@gmail.com
// Observar que se pueden establecer rangos de valores utilizando la siguient estrategia: No se coloca un break, 
// y por lo tanto para varios valores distintos se va a hacer exactamente lo mismo.

public class SwitchStatement{
	public static void main(String[] args){
		char car = 'o';

		switch (car) {
			case 'a':
			case 'e':
			case 'i':
			case 'o':
			case 'u':{
				System.out.println("El caracter es una vocal");
				break;
			}
			default:{
				System.out.println("El caracter no es una vocal");
				break;
			}
		}
	}
}