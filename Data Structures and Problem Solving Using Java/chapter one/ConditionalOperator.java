// Uso del operador condicional
// Páginas 17 y 19 del libro Data Structures & Problem Solving Using Java
// etcheverrypablol@gmail.com
// testExpr ? yesExpr : noExpr

public class ConditionalOperator{
	public static void main(String[] args){
		int num = 1;
		String result = "";
		result = num == 0 ? (num + " es cero") : (num + " No es cero");
		System.out.println(result);
		
		// i < 0 ? result = "Es cero" : result = "No es cero"; (MAL) 
		
		int x = 1; int y = 9;
		int min = min(x, y);
		System.out.println("El menor entre " + x + " y " + y + " es: " + min);
	}

	public static int min(int x, int y){
		return  x < y ? x : y;
	}
}