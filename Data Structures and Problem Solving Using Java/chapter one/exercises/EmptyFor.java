// Use of the empty for loop
// Page 24 of the book Data Structures & Problem Solving Using Java
// etcheverrypablol@gmail.com

// Write a while statement that is wquivalent to the following for fragment. Why would this useful?
// for ( ; ; )
// 	statement

// Answer: The empty for is equivalent to while whit a test true, wich is a infinity loop
// This is useful if you wish to run the program indefinitly

public class EmptyFor{
	public static void main(String[] args){
		// If you wish to leave of the cycle, you can use the following condition  
		boolean leave_the_cycle = true;
		int i = 0;
		for( ; ; ){
			System.out.println("This is an empty for");
			if(leave_the_cycle){
				break;
			}
		}
	}
}