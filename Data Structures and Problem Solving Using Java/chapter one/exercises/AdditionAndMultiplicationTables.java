// Adition and multiplication tables
// Page 24 of the book Data Structures & Problem Solving Using Java
// etcheverrypablol@gmail.com

// Write a program to generate the addition and multiplication tables for single-digit 
// numbers (the table that elementary school students are accustomed to seeing).

public class AdditionAndMultiplicationTables{
	public static void main(String[] args){
		int num = Integer.parseInt(args[0]);
		
		System.out.println("ADDITION AND MULTIPLICATION TABLES: " + args[0]);
		for (int i = 0; i<11; i++) {
			System.out.println(num + " x " + i + " = " + num*i);
		}
		System.out.println();

		for (int i = 0; i<11; i++) {
			System.out.println(num + " + " + i + " = " + (num+i)); // I have to put the parentheses so that the sum sign is considered to add and not concatenate
		}
		System.out.println();
	}
}