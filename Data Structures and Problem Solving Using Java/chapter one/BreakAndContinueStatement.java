// Uso de la sentencia break con etiquetas
// Páginas 16 y 17 del libro Data Structures & Problem Solving Using Java
// etcheverrypablol@gmail.com

public class BreakAndContinueStatement{
	
	public static void main(String[] args) {
		
        // USO DE LA SENTENCIA BREAK
        int i = 0;
		while( i < 10){
			System.out.println(i);
			i++;
			if(i == 3){
				break;	// Sale del ciclo por más que no se haya terminado el ciclo
			}
		}
		
        // USO DE ETIQUETAS PARA EL BREAK
        i = 0;
		outer:
		while(i < 20){
			while(i<10){
				System.out.println(i);
				if(i == 3){
					break outer; // Sale luego del while más externo donde está la etiqueta outer
				}
				i++;	
			}
			i++;	
		}

		int counter = 0;
        // volver a middle significa que se corte el flujo y se pase a la siguiente iteración del for más externo
        outer:
        for(i = 0; i < 3; i++){
            System.out.println("middle: i: "+i);
            middle:
            for(int j = 0; j < 3; j++){
                System.out.println("inner: j: "+j);   
                inner:
                for(int k = 0; k < 3; k++){            
                    System.out.println("for k.. : "+k);
                    if(k - j > 0){
                        System.out.println("break midle:");
                        break middle;
                    }
                    counter++;
                    System.out.println("counter: "+counter);
                }
            }
        }
        System.out.println(counter);
        
        // USO DE SENTIENCIA continue
        System.out.println("Se imprimen todos los números del 1 al 100 excepto los que son múltiplos de 10");
        for (i = 0; i <= 100; i++) {
        	if (i % 10 == 0) {
        		continue; // Lo que hace es cortar el ciclo y pasar al siguiente
        	}
        	System.out.print(i+ " ");
        }
        System.out.println("\n");
	}
}